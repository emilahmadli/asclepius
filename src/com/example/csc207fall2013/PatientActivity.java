package com.example.csc207fall2013;

import java.util.Date;

import Internal.*;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class PatientActivity extends Activity {

	private User user;
	private Patient patient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		user = (User) thisintent.getExtras().get("user");
		String p_name = (String) thisintent.getExtras().get("patient");
		patient = user.searchPatient(p_name);
		setContentView(R.layout.activity_patient);
		Button addSymBtn = (Button) findViewById(R.id.addSymBtn);
		Button addVitalBtn = (Button) findViewById(R.id.addVitalBtn);
		Button addPresBtn = (Button) findViewById(R.id.addPresBtn);
		Button CheckBtn = (Button) findViewById(R.id.buttonChecked);
		if (user.getClass() == Nurse.class) {
			addPresBtn.setVisibility(4);
		}
		else {
			addSymBtn.setVisibility(4);
			addVitalBtn.setVisibility(4);
			CheckBtn.setVisibility(4);
		}
		TextView dob = (TextView) findViewById(R.id.PatientDobField);
		dob.setText(patient.getDoB().toString());
		ListView vitals = (ListView) findViewById(R.id.listVital);
		ListView symptoms = (ListView) findViewById(R.id.listSymptom);
		ListView pres = (ListView) findViewById(R.id.listPres);
		updateLists();
		vitals.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ListView Clicked item value
				ListView vitals = (ListView) parent;
				Vital itemValue = (Vital) vitals.getItemAtPosition(position);
				AlertDialog.Builder dlgAlert  = 
						new AlertDialog.Builder(parent.getContext());
				dlgAlert.setMessage(itemValue.getString());
				dlgAlert.setTitle("Vital details");
				dlgAlert.setPositiveButton("Ok",
					    new DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog,
					        		int which) {
					          //dismiss the dialog  
					        }
					    });
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();
				}
			});
		
		symptoms.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ListView Clicked item value
				ListView symptoms = (ListView) parent;
				Date date = (Date) symptoms.getItemAtPosition(position);
				String itemValue = (String) patient.getSymptoms().get(
						symptoms.getItemAtPosition(position));
				AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(
						parent.getContext());
				dlgAlert.setMessage(itemValue);
				dlgAlert.setTitle("Symptom at " + date.toString());
				dlgAlert.setPositiveButton("Ok",
					    new DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, 
					        		int which) {
					          //dismiss the dialog  
					        }
					    });
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();
				// Show Alert
				}
			});
		
		pres.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ListView Clicked item value
				ListView pres = (ListView) parent;
				Date date = (Date) pres.getItemAtPosition(position);
				String itemValue = (String) patient.getPrescription().get(
						pres.getItemAtPosition(position));
				AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(
						parent.getContext());
				dlgAlert.setMessage(itemValue);
				dlgAlert.setTitle("Prescription at " + date.toString());
				dlgAlert.setPositiveButton("Ok",
					    new DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, 
					        		int which) {
					          //dismiss the dialog  
					        }
					    });
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();
				// Show Alert
				}
			});
		}
	
	/**
	 * Updates lists of vital signs and symptoms every time the new
	 * vital signs or symptoms or prescriptions are added 
	 * Also updates the urgency level and status
	 */
	protected void updateLists() {
		ListView vitals = (ListView) findViewById(R.id.listVital);
		ListView symptoms = (ListView) findViewById(R.id.listSymptom);
		ListView pres = (ListView) findViewById(R.id.listPres);
		ArrayAdapter<Object> adapterVital = 
				new ArrayAdapter<Object>(this.getApplicationContext(),
				 R.layout.vitalitem, patient.getVitals().values().toArray()
		 );
		ArrayAdapter<Object> adapterSym = 
				new ArrayAdapter<Object>(this.getApplicationContext(),
				 R.layout.vitalitem, patient.getSymptoms().keySet().toArray()
		 );
		ArrayAdapter<Object> adapterPres = 
				new ArrayAdapter<Object>(this.getApplicationContext(),
				 R.layout.vitalitem, patient.getPrescription().keySet().toArray()
		 );
		vitals.setAdapter(adapterVital);
		symptoms.setAdapter(adapterSym);
		pres.setAdapter(adapterPres);
		TextView name = (TextView) findViewById(R.id.patientName);
		name.setText(patient.getName() + ", Urgency Level " + 
		patient.getUrgencyLevel() + "["
				+ patient.getUrgencyCategory() + "]");
		TextView seen = (TextView) findViewById(R.id.txtSeenData);
		Button CheckBtn = (Button) findViewById(R.id.buttonChecked);
		if (patient.getStatus()) {
			seen.setText("Checked at " + patient.checkedTime());
			CheckBtn.setEnabled(false);
			CheckBtn.setText("Patient checked");
		}
		else {
			seen.setText("Unchecked");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}

	/**
	 * Logs out the user
	 * @param view the activity of the user
	 */
	public void logOut(View view) {
		this.finishActivity(42);
	}

	/**
	 * Adds vitals to the Patient
	 * @param view the Vital Signs input by the Nurse
	 */
	public void addVital(View view) {
		if (patient != null) {
			Intent addV = new Intent(this, AddVitalActivity.class);
			addV.putExtra("Patient", patient.getHealthID());
			addV.putExtra("user", user);
			this.startActivityForResult(addV, 99);
		}
	}

	/**
	 * Adds symptoms to the Patient
	 * @param view the symptoms input
	 */
	public void addSymptom(View view) {
		if (patient != null) {
			Intent addS = new Intent(this, AddSymptomActivity.class);
			addS.putExtra("Patient", patient.getHealthID());
			addS.putExtra("user", user);
			this.startActivityForResult(addS, 99);
		}
	}

	/**
	 * Adds prescription to the Patient
	 * @param view the prescription input
	 */
	public void addPres(View view) {
		if (patient != null) {
			Intent addP = new Intent(this, AddPresActivity.class);
			addP.putExtra("Patient", patient.getHealthID());
			addP.putExtra("user", user);
			this.startActivityForResult(addP, 99);
		}
	}
	
	/**
	 * Marks the current patient as checked by doctor
	 * @param view
	 */
	public void markDoc(View view) {
		if (patient != null) {
			((Nurse) user).patientSeenByDoc(patient);
			this.updateLists();
		}
	}
	
	/**
	 * Everytime a vital or a symptom or a prescription is 
	 * added the list corresponding to them is updated
	 */
	protected void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        if (requestCode == 99) {
            this.updateLists();
        }
	}

	/** Directs to main screen when back is pressed, when the Nurse
	 * is done creating the Patient
	 */
	public void goBack(View view) {
		this.finish();
	}
}
