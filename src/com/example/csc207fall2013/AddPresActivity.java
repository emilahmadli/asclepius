package com.example.csc207fall2013;

import Internal.Patient;
import Internal.Physician;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class AddPresActivity extends Activity {

	private Patient p;
	private Physician user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		String p_name = (String) thisintent.getExtras().get("Patient");
		user = (Physician) thisintent.getExtras().get("user");
		p = user.searchPatient(p_name);
		setContentView(R.layout.presformactivity);
		TextView tv = (TextView) findViewById(R.id.textPatientName);
		tv.setText(p.getName());
	}

	/**
	 * Allows the nurse to confirm the action of adding a prescription 
	 * to a patient
	 * @param view
	 */
	public void acceptPrescription(View view) {
		EditText name = (EditText) findViewById(R.id.medNameField);
		EditText desc = (EditText) findViewById(R.id.instructionField);
		user.addPrescription(p, name.getText().toString(), desc.getText().toString());
		this.finish();

	}

	/**
	 * Allows the nurse user to go back to main menu
	 * @param view          
	 */
	public void goBack(View view) {
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
}
