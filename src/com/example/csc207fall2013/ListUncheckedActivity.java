package com.example.csc207fall2013;

import java.util.List;

import android.os.Bundle;
import Internal.*;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ListUncheckedActivity extends Activity {
	
	public Nurse user;
	public List<Patient> unchecks;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent this_intent = this.getIntent();
		user = (Nurse) this_intent.getExtras().get("user");
		setContentView(R.layout.activity_list_unchecked);
		updateLists();
		ListView unch = (ListView) findViewById(R.id.uncheckedList);
		unch.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ListView Clicked item value
				ListView pres = (ListView) parent;
				Patient pat = (Patient) pres.getItemAtPosition(position);
				Intent patIntent = new Intent(view.getContext(),
						PatientActivity.class);
				patIntent.putExtra("patient", pat.getHealthID());
				patIntent.putExtra("user", user);
				startActivity(patIntent);
				((ListUncheckedActivity) parent.getContext()).finish();
				}
			});
	}
	
	/**
	 * Updates the list of unchecked patients
	 */
	protected void updateLists() {
		unchecks = user.listPatientsUnchecked();
		ListView unch = (ListView) findViewById(R.id.uncheckedList);
		ArrayAdapter<Object> adapterUn = 
				new ArrayAdapter<Object>(this.getApplicationContext(),
				 R.layout.vitalitem, unchecks.toArray()
		 );
		unch.setAdapter(adapterUn);
		TextView nums = (TextView) findViewById(R.id.unPatNum);
		nums.setText(adapterUn.getCount() + " ");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_unchecked, menu);
		return true;
	}
}
