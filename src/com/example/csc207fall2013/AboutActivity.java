package com.example.csc207fall2013;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class AboutActivity extends Activity {
	/**
	 * Set about activity's layout
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
	}
	/**
	 * Creating options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}

}
