package com.example.csc207fall2013;

import Internal.Nurse;
import Internal.Patient;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class AddSymptomActivity extends Activity {

	private Patient p;
	private Nurse user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		String p_name = (String) thisintent.getExtras().get("Patient");
		user = (Nurse) thisintent.getExtras().get("user");
		p = user.searchPatient(p_name);
		setContentView(R.layout.symptomformactivity);
		TextView tv = (TextView) findViewById(R.id.textPatientName);
		tv.setText(p.getName());
	}

	/**
	 * Allows the nurse to confirm the action of adding a symptom 
	 * to a patient
	 * @param view         
	 */
	public void acceptSymptom(View view) {
		EditText desc = (EditText) findViewById(R.id.symField);
		user.recordSymptoms(p, desc.getText().toString());
		this.finish();

	}

	/**
	 * Allows the nurse user to go back to main menu
	 * @param view          
	 */
	public void goBack(View view) {
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
}
