package com.example.csc207fall2013;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class MainActivity extends Activity {
	/**
	 * Set Activity's layout
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);
	}
	/**
	 * Creating options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it 
		// is present.
		return true;
	}
	/**
	 * If user clicks Login button it'll go LoginActivity
	 */
	public void goLogin(View view) {
		Intent loginIntent = new Intent(this, LoginActivity.class);
		this.startActivityForResult(loginIntent, 42);
	}
	/**
	 * If user clicks Help button it'll go AelpActivity
	 */
	public void goHelp(View view) {
		Intent helpIntent = new Intent(this, HelpActivity.class);
		startActivity(helpIntent);
	}
	/**
	 * If user clicks About button it'll go AboutActivity
	 */
	public void goAbout(View view) {
		Intent aboutIntent = new Intent(this, AboutActivity.class);
		startActivity(aboutIntent);
	}
}
