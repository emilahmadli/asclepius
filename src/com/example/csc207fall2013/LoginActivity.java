package com.example.csc207fall2013;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Internal.Nurse;
import Internal.Physician;
import Internal.Record;
import Internal.User;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class LoginActivity extends Activity {
	
	public Record appRecord;
	/**
	 * Set Login activity's layout
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		loadRecord();
	}
	/**
	 * Creating options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
	/**
	 * Saving all data to the records.sav file
	 */
	public void saveRecord()
	{
		if (appRecord == null) return;
		try {
			FileOutputStream  fileStream = openFileOutput("records.sav", 
					LoginActivity.MODE_PRIVATE);
			ObjectOutputStream output = new ObjectOutputStream(fileStream);
			output.writeObject(appRecord);
			output.close();
		} catch (IOException ex) {
			System.out.println("Cannot perform output." + ex);
		}
	}
	/**
	 * Loading all data from records.sav file
	 */
	public void loadRecord()
	{
		try {
			FileInputStream file = this.openFileInput("records.sav");
			ObjectInput input = new ObjectInputStream(file);
			// deserialize the List
			appRecord = (Record) input.readObject();
			Nurse.setRecordTarget(appRecord);
			input.close();
		} catch (ClassNotFoundException ex) {
			System.out.println("Cannot perform input. Class not found." + ex);
		} catch (IOException ex) {
			System.out.println("Cannot perform input." + ex);
		}
		if (appRecord == null) 
			{
			appRecord = new Record();
			appRecord.addNurse(new Nurse("nurse", "nurse"));
			appRecord.addPhysician(new Physician("phy", "phy"));
			}
	}
	/**
	 * Checks if login and password correct for nurse
	 */
	public void nurseLogin(View view) {
		EditText username_field = (EditText) findViewById(R.id.usernameField);
		EditText password_field = (EditText) findViewById(R.id.passwordField);
		String username_text = username_field.getText().toString();
		String password_text = password_field.getText().toString();
		if (username_text.length() < 1 || password_text.length() < 1)
		{
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Please enter information in both " +
					"username and password field!");
			dlgAlert.setTitle("Login error!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, 
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return;
		}
		User user;
		if (!appRecord.getUserList().containsKey(username_text))
		{
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("User does not exist!");
			dlgAlert.setTitle("Login error!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog,
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return;
		}
		else
		{
			user = appRecord.getUserList().get(username_text);
		}
		if (user.login(password_text))
		{
			Intent intent;
			if (user.getClass() == Nurse.class) {
				intent = new Intent(this, NurseActivity.class);
			}
			else {
				intent = new Intent(this, PhyActivity.class);
			}
			intent.putExtra("user", user);
			intent.putExtra("Record", appRecord);
			startActivity(intent);
			this.finish();
			
		}
		else
		{

			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Invalid Password!");
			dlgAlert.setTitle("Login error!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, 
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return;
		}
	}
	/**
	 * Creating new nurse 
	 */
	public void createNurseClick(View view) {
		EditText username_field = (EditText) findViewById(R.id.usernameField);
		EditText password_field = (EditText) findViewById(R.id.passwordField);
		String username_text = username_field.getText().toString();
		String password_text = password_field.getText().toString();
		if (username_text.length() == 0) return;
		if (appRecord.getUserList().containsKey(username_text))
		{
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("User already exist!");
			dlgAlert.setTitle("Nurse account creation error!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog,
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
		}
		else
		{
			appRecord.addNurse(new Nurse(username_text, password_text));
			saveRecord();
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Successfully created account " + 
			username_text);
			dlgAlert.setTitle("Account created!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, 
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
		}
	}
	/**
	 * Creating new physician 
	 */
	public void createPhyClick(View view) {
		EditText username_field = (EditText) findViewById(R.id.usernameField);
		EditText password_field = (EditText) findViewById(R.id.passwordField);
		String username_text = username_field.getText().toString();
		String password_text = password_field.getText().toString();
		if (username_text.length() == 0) return;
		if (appRecord.getUserList().containsKey(username_text))
		{
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("User already exist!");
			dlgAlert.setTitle("Physician account creation error!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog,
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
		}
		else
		{
			appRecord.addPhysician(new Physician(username_text, password_text));
			saveRecord();
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Successfully created account " + 
			username_text);
			dlgAlert.setTitle("Account created!");
			dlgAlert.setPositiveButton("Ok",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, 
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
		}
	}
}
