package com.example.csc207fall2013;


import java.util.Date;

import Internal.Nurse;
import Internal.Patient;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class Create_patient extends Activity {
	
	private Nurse user;
	private Date date;
	private String name;
	private String healthID; 
	private Patient p;
	
	/**
	 * Set the layout of creating new patient activity
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		user = (Nurse) thisintent.getExtras().get("user");		
		setContentView(R.layout.activity_create_patient);
	}
	/**
	 * Creating options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
	/**
	 * Accepting (Adding) new patient with Health CardID, name and date 
	 * of birth
	 */
	@SuppressWarnings("deprecation")
	public void acceptPatient (View view){
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		date = new Date(dp.getYear() - 1900, dp.getMonth(), 
				dp.getDayOfMonth());
		EditText tempName = (EditText) findViewById(R.id.patientField);
		EditText temphealthID = (EditText) findViewById(R.id.healthID);
		name = tempName.getText().toString();
		healthID = temphealthID.getText().toString();
		user.addPatient(name, date, healthID);
		//this.finish()
		p = user.searchPatient(healthID);
		Intent addProfile = new Intent(this, PatientActivity.class);
		addProfile.putExtra("user", user);
		addProfile.putExtra("patient", p.getHealthID());
		startActivity(addProfile);
		this.finish();
		}
	}
