package com.example.csc207fall2013;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import Internal.Nurse;
import Internal.Record;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class NurseActivity extends Activity {
	
	private Nurse user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent this_intent = this.getIntent();
		user = (Nurse) this_intent.getExtras().get("user");
		setContentView(R.layout.activity_nurse);
		TextView title = (TextView) findViewById(R.id.nurseName);
		title.setText(user.getUsername());
	}
	
	/** Creates and adds Patient */
	public void addPatient(View view) {
		Intent addPat = new Intent(this, Create_patient.class);
		addPat.putExtra("user", user);
		startActivity(addPat);
	}
	
	/** Lists all Patients */
	public void listPatient(View view) {
		Intent listPat = new Intent(this, SearchPatientActivity.class);
		listPat.putExtra("user", user);
		startActivity(listPat);
	}
	
	/** Lists all unchecked Patients */
	public void listUncheckedPatient(View view) {
		Intent listUnPat = new Intent(this, ListUncheckedActivity.class);
		listUnPat.putExtra("user", user);
		startActivity(listUnPat);
	}
	
	/** Saves data */
	public void saveData(View view) throws IOException {
		Record record = Nurse.getRecord();
		if (record == null) return;
		FileOutputStream  fileStream = openFileOutput("records.sav",
				NurseActivity.MODE_PRIVATE);
		ObjectOutputStream output = new ObjectOutputStream(fileStream);
		output.writeObject(record);
		output.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.nurse, menu);
		return true;
	}
}
