package com.example.csc207fall2013;

import Internal.Nurse;
import Internal.Patient;
import Internal.Vital;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class AddVitalActivity extends Activity {
	private Patient p;
	private Nurse user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		String p_name = (String) thisintent.getExtras().get("Patient");
		user = (Nurse) thisintent.getExtras().get("user");
		p = user.searchPatient(p_name);
		setContentView(R.layout.symptomformactivity);
		TextView tv = (TextView) findViewById(R.id.textPatientName);
		tv.setText(p.getName());
		setContentView(R.layout.vitalform);
		EditText temp = (EditText) findViewById(R.id.tempField);
		EditText blood1 = (EditText) findViewById(R.id.blood1Field);
		EditText blood2 = (EditText) findViewById(R.id.blood2Field);
		EditText heart = (EditText) findViewById(R.id.heartField);
		Vital lastVital;
		if (p.getVitals().size() > 0) {
			lastVital = (Vital) p.getVitals().values().toArray()[0];
			temp.setText(lastVital.getTemperature() + "");
			blood1.setText(lastVital.getBloodPressureSystolic() + "");
			blood2.setText(lastVital.getBloodPressureDiastolic() + "");
			heart.setText(lastVital.getHeartRate() + "");
			Toast.makeText(
					this,
					"Most recent vital data loaded.\nModify the fields " +
					"that needs to be updated.",
					Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * Allows the nurse to confirm the action of adding a vital
	 * to a specific patient
	 * @param view 
	 */
	public void acceptVital(View view) {
		EditText temp = (EditText) findViewById(R.id.tempField);
		EditText blood1 = (EditText) findViewById(R.id.blood1Field);
		EditText blood2 = (EditText) findViewById(R.id.blood2Field);
		EditText heart = (EditText) findViewById(R.id.heartField);
		if (blood1.getText().toString().length() < 1
				|| blood2.getText().toString().length() < 1
				|| temp.getText().toString().length() < 1
				|| heart.getText().toString().length() < 1) {

			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Please fill out all the fields!");
			dlgAlert.setTitle("Invalid input!");
			dlgAlert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, 
								int which) {
							// dismiss the dialog
						}
					});
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return;
		}
		double blood1_value = Double.parseDouble(blood1.getText().toString());
		double blood2_value = Double.parseDouble(blood2.getText().toString());
		double temp_value = Double.parseDouble(temp.getText().toString());
		double heart_value = Double.parseDouble(heart.getText().toString());
		user.recordVital(p, temp_value, blood1_value, blood2_value, 
				heart_value);
		this.finish();
	}

	/**
	 * Allows the nurse to go back the main menu
	 * @param view
	 */
	public void goBack(View view) {
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
}
