package com.example.csc207fall2013;

import Internal.Patient;
import Internal.User;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class SearchPatientActivity extends Activity {

	private User user;
	private Patient patient;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent thisintent = this.getIntent();
		user = (User) thisintent.getExtras().get("user");
		setContentView(R.layout.activity_list_patient);
	}
	
	/**
	 * Allows the user to finish interacting with the searching 
	 * activity
	 * @param view 
	 */
	public void logOut(View view) {
		this.finishActivity(42);
	}
	
	/**
	 * Allows the nurse to search for a patient
	 * @param view
	 */
	public void searchPatient(View view) {
		EditText h_num = (EditText) findViewById(R.id.healthCardnumField);
		String h_value = h_num.getText().toString();
		patient = user.searchPatient(h_value);
		if (patient != null)
		{
			Intent patIntent = new Intent(this, PatientActivity.class);
			patIntent.putExtra("patient", patient.getHealthID());
			patIntent.putExtra("user", user);
			startActivity(patIntent);
			this.finish();
		}
		else
		{
			AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
			dlgAlert.setMessage("Patient with this health ID does not " +
					"exist in the database!");
			dlgAlert.setTitle("Unable to find patient!");
			dlgAlert.setPositiveButton("OK",
				    new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, 
				        		int which) {
				          //dismiss the dialog  
				        }
				    });
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		return true;
	}
}
