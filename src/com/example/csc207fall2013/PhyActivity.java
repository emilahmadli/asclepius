package com.example.csc207fall2013;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import Internal.Physician;
import Internal.Record;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

/** @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. */
public class PhyActivity extends Activity {
	
	private Physician user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent this_intent = this.getIntent();
		user = (Physician) this_intent.getExtras().get("user");
		setContentView(R.layout.activity_phy);
		TextView title = (TextView) findViewById(R.id.nurseName);
		title.setText(user.getUsername());
	}
	
	/** Lists all Patients */
	public void listPatient(View view) {
		Intent listPat = new Intent(this, SearchPatientActivity.class);
		listPat.putExtra("user", user);
		startActivity(listPat);
	}
	
	/** Saves data */
	public void saveData(View view) throws IOException {
		Record record = Physician.getRecord();
		if (record == null) return;
		FileOutputStream  fileStream = openFileOutput("records.sav",
				PhyActivity.MODE_PRIVATE);
		ObjectOutputStream output = new ObjectOutputStream(fileStream);
		output.writeObject(record);
		output.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.nurse, menu);
		return true;
	}
}
