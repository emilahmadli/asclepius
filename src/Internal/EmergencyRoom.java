package Internal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;

/**
 * Implements the class EmergencyRoom
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew.
 */
public class EmergencyRoom implements Serializable {

	/**
	 * Initializes the variables.
	 */
	private static final long serialVersionUID = 6887376212971382179L;
	// The String in this HashMap is the Health Care ID of a patient.
	protected Map<String, Patient> patientsInER;
	protected List<Patient> urgencyList;
	protected List<Patient> arrivalList;
	
	/** 
	 * Constructs the class EmergencyRoom.
	 */
	public EmergencyRoom() {
		patientsInER = new HashMap<String, Patient>();

	}
	
	/**
	 * Compares the arrival time of two patients.
	 */
	public class arrivalComparator implements Comparator<Patient> {
		/**
		 * Compares the arrival time of two patients.
		 * @param p1 The patient to be compared.
		 * @param p2 The patient to be compared.
		 * @return The patient arrived earlier.
		 */
		@Override
		public int compare(Patient p1, Patient p2) {
			return p1.getArrivalTime().compareTo(p2.getArrivalTime());
		}
	}

	// Compare urgencyLevel between two patients
	public class urgencyComparator implements Comparator<Patient> {
		@Override
		public int compare(Patient p1, Patient p2) {

			// First compare patient 1's urgencyLevel with patient 2's.
			if (p1.getUrgencyLevel() > p2.getUrgencyLevel()) {
				return -1;
			} else if (p1.getUrgencyLevel() < p2.getUrgencyLevel()) {
				return 1;
			} else {

				// If two patients have the same urgencyLevel, then compare 
				// them by arrival time and date.
				return p1.getArrivalTime().compareTo(p2.getArrivalTime());
			}
		}
	}
	
	/**
	 * Returns the list of Patients sorted in non decreasing order
	 * by their urgency index.
	 * @return a list of Patient objects sorted by urgency index.
	 */
	public List<Patient> listByUrgency() {
		urgencyList = (List<Patient>) patientsInER.values();
		Collections.sort(urgencyList, new urgencyComparator());
		return urgencyList;
	}
	
	/**
	 * Returns the list of Patients sorted in non decreasing order
	 * by their arrival time.
	 * @return a list of Patients sorted by arrival time.
	 */
	public List<Patient> listByArrival() {
		arrivalList = (List<Patient>) patientsInER.values();
		Collections.sort(arrivalList, new arrivalComparator());
		return arrivalList;
	}

	/**
	 * Returns a list where every element is a string representation of 
	 * the original element input.
	 * @param list inputs a list of Patient objects.
	 * @return a list where every element is a string representation of 
	 * the original element input.
	 */
	public List<String> toStringList(List<Patient> list) {
		List<String> toStringList = new ArrayList<String>();
		for (Patient p : list) {
			toStringList.add(p.toString());
		}
		return toStringList;
}
	
	/**
	 * Adds a patient to Emergency Room.
	 * @param p allows a patient to be added into a emergencyRoom.
	 */
	// Add a new patient to EmergencyRoom if he hasn't been checked
	public void addPatientToER(Patient p) {
		if (!p.getStatus()) {
			patientsInER.put(p.getHealthID(), p);
		}
	}
	
	/**
	 * Returns the the Patient object corresponding to the healthID.
	 * @param keyword this is the healthID of the patient.
	 * @return returns the Patient object corresponding to the healthID.
	 */
	public Patient searchById(String keyword) {
		return patientsInER.get(keyword);
	}
	
	/**
	 * Removes the given Patient from Emergency Room.
	 * @param p removes the input patient from our patientsInER hashmap.
	 */
	// Check out a patient and set his seenByDoctor status true;
	public void patientCheckOut(Patient p) {
		p.setStatus(true);
		patientsInER.remove(p.getHealthID());

	}
}
