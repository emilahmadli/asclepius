package Internal;

import java.io.Serializable;
import java.util.Date;

/**
 * Implementing the class Vital.
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew.
 */
public class Vital implements Serializable {
	/**
	 * Initializes the variables.
	 */
	private static final long serialVersionUID = -6571968860298072298L;
	private double temperature, heartRate, bloodPressure1, bloodPressure2;
	private Date time;

	/**
	 * Constructs the class Vital.
	 * @param temp The temperature of the Patient.
	 * @param blood1 The systolic blood pressure of the Patient.
	 * @param blood2 The diastolic blood pressure of the Patient.
	 * @param heart The heart rate of the Patient.
	 */
	public Vital(double temp, double blood1, double blood2, double heart) {
		this.temperature = temp;
		this.bloodPressure1 = blood1;
		this.bloodPressure2 = blood2;
		this.heartRate = heart;
		this.time = new Date();
	}

	/**
	 * Returns the time when the vital sign is recorded.
	 * @return the time when a vital sign is recorded.
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * Returns the heart rate.
	 * @return the heartRate when a vital is recorded.
	 */
	public double getHeartRate() {
		return heartRate;
	}

	/**
	 * Sets heart rate at a particular time.
	 * @param heartRate at a particular time.
	 */
	public void setHeartRate(double heartRate) {
		this.heartRate = heartRate;
	}

	/**
	 * Returns the temperature.
	 * @return the patient's temperature.
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature.
	 * @param temperature of the patient.
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	/**
	 * Returns the systolic blood pressure. 
	 * @return the systolic bloodPressure.
	 */
	public double getBloodPressureSystolic() {
		return bloodPressure1;
	}

	/**
	 * Returns the diastolic blood pressure.
	 * @return the diastolic bloodPressure.
	 */
	public double getBloodPressureDiastolic() {
		return bloodPressure2;
	}

	/**
	 * Sets the systolic blood pressure.
	 * @param the systolic bloodPressure.
	 */
	public void setBloodPressureSystolic(double bloodPressure) {
		this.bloodPressure1 = bloodPressure;
	}

	/**
	 * Sets diastolic blood pressure.
	 * @param the diastolic bloodPressure.
	 *            
	 */
	public void setBloodPressureDiastolic(double bloodPressure) {
		this.bloodPressure2 = bloodPressure;
	}

	/**
	 * Returns a string that shows the time when the vital was recorded.
	 * @return a string that shows the time when the vital was recorded.
	 */
	public String toString() {
		return "Vital at " + getTime().toString();
	}

	/**
	 * Returns a string representation of all vital signs and the time.
	 * @return a string representation of all vital signs and the time.
	 */
	public String getString() {
		return "Date: " + getTime().toString() + "\nHeart rate: "
				+ this.heartRate + "\nBlood Pressure: " + this.bloodPressure1
				+ " / " + this.bloodPressure2 + "\nTemperature: "
				+ this.temperature;
	}
}
