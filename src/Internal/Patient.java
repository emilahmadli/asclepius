package Internal;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import android.text.format.Time;

/**
 * Implementing the class Patient.
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew.
 */
public class Patient implements Serializable {

	/** Initializing the variables. */
	/** A unique ID for serialization. */
	private static final long serialVersionUID = -5602453016937032263L;
	private String name;
	private String healthID;
	private int urgencyLevel;
	private String arrivalTime;
	private Boolean status;
	private Map<Date, String> symptoms;
	private Map<Date, Vital> vitals;
	private Map<Date, String> prescriptions;
	private String seenByDocTime;
	private Date dob;
	private String urgencyCategory;
	
	/**
	 * Constructs a Patients using the name, date of birth,
	 * and Health card number.
	 * @param name Patient's name.
	 * @param dob Patient's date of birth.
	 * @param healthID Patient's health card number. 
	 */
	public Patient(String name, Date dob, String healthID) {
		this.name = name;
		this.healthID = healthID;
		Time now = new Time();
		now.setToNow();
		this.arrivalTime = now.toString();
		this.status = false;
		this.vitals = new LinkedHashMap<Date, Vital>();
		this.symptoms = new LinkedHashMap<Date, String>();
		this.prescriptions = new LinkedHashMap<Date, String>();
		this.dob = dob;
		this.urgencyLevel = 0;
		this.urgencyCategory = "Undefined";
	}
	
	/**
	 * Returns the Patient's urgency category: 
	 * urgent, less urgent, or non urgent.
	 * @return the Patient's urgency category.
	 */
	public String getUrgencyCategory() {
		return urgencyCategory;
	}

	/**
	 * Returns Urgency index of the Patient.
	 * @return The urgency index of the Patient.
	 */
	public int getUrgencyLevel() {
		return urgencyLevel;
	}

	/**
	 * Sets the urgency index of the Patient.
	 * @param urgency The urgency index of the Patient.
	 */
	public void setUrgencyLevel(int urgency) {
		this.urgencyLevel = urgency;
	}

	/**
	 * Returns the Date of Birth of the Patient.
	 * @return the Date of Birth of the Patient.
	 */
	public Date getDoB() {
		return dob;
	}

	/**
	 * Returns the Arrival time of the Patient.
	 * @return the Arrival time of the Patient.
	 */
	public String getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * Returns the String representation of the Patient.
	 */
	public String toString() {
		return getUrgencyLevel() + " - " + name;
	}

	/**
	 * Returns the Health Card Number of the Patient.
	 * @return the Health Card Number of the Patient.
	 */
	public String getHealthID() {
		return healthID;
	}

	/**
	 * Sets whether the Patient is checked by Doctor or not.
	 * @param s True if the patient is checked and false otherwise.
	 */
	public void setStatus(Boolean s) {
		this.status = s;
	}

	/**
	 * Returns the Patient's status, whether the Patient
	 * is checked by doctor or not.
	 * @return True if the patient is checked and false otherwise.
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Marks the patient as already checked by doctor.
	 */
	public void checkedBy() {
		Date now = new Date();
		this.seenByDocTime = now.toString();
		this.setStatus(true);
		}
	
	/**
	 * Gets the time the patient is seen by the doctor.
	 * @return The time the patient is checked in String.
	 */
	public String checkedTime() {
		return seenByDocTime;
	}

	/**
	 * Adds the vitals signs of the Patient, temperature,
	 * blood pressure(systolic/diastolic), and heart rate
	 * @param temp The temperature of the Patient.
	 * @param pres1 The systolic blood pressure of the Patient.
	 * @param pres2 The diastolic blood pressure of the Patient.
	 * @param heart The heart rate of the Patient.
	 */
	public void addVital(double temp, double pres1, 
			double pres2, double heart) {
		Vital newVital = new Vital(temp, pres1, pres2, heart);
		this.calculateUrgency(newVital);
		vitals.put(newVital.getTime(), newVital);
	}
	
	/**
	 * Adds a new prescription information to the patient's record.
	 * @param medication The medication name of the prescription
	 * @param instruction The instruction of the prescription
	 */
	public void addPrescription(String medication, String instruction) {
		Date now = new Date();
		this.prescriptions.put(now, "Medication: " + medication + "\nInstructions: " 
		+ instruction);
	}

	/**
	 * Returns the Map of prescriptions, the Date as the keys
	 * and the Prescriptions as the values.
	 * @return the prescriptions map of the Patient.
	 */
	public Map<Date, String> getPrescription() {
		return prescriptions;
	}

	/**
	 * Returns the map of Vital signs of the Patient,
	 * Date as the key and the Vital signs as the values.
	 * @return the map of Vital signs of the Patient.
	 */
	public Map<Date, Vital> getVitals() {
		return vitals;
	}

	/**
	 * Returns the Vital sign at the specific time.
	 * @param time The specific time of the Vital signs.
	 * @return the Vital sign at the specific time.
	 */
	public Vital getVitalsByTime(String time) {
		return vitals.get(time);
	}

	/**
	 * Returns the name of the Patient.
	 * @return the name of the Patient.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the Map of symptoms, the Date as the keys
	 * and the Symptoms as the values.
	 * @return the symptoms of the Patient.
	 */
	public Map<Date, String> getSymptoms() {
		return symptoms;
	}

	/**
	 * Sets the symptoms for the Patient.
	 * @param symptom The symptom of the Patient.
	 */
	public void setSymptoms(String symptom) {
		Date now = new Date();
		this.symptoms.put(now, symptom);
	}

	/**
	 * Calculates the urgency level based on the vital signs
	 * of the Patient.
	 * @param vital The vital signs of the Patient.
	 */
	@SuppressWarnings("deprecation")
	public void calculateUrgency(Vital vital) {
		// Set urgency level to 0
		urgencyLevel = 0;
		// Check if the Patient is younger than 2 years
		Date now = new Date();
		int current_year = now.getYear();
		if ((current_year - dob.getYear()) < 2) {
			urgencyLevel++;
		}
		// Check if the temperature of the Patient is over 39.0
		if (vital.getTemperature() >= 39.0) {
			urgencyLevel++;
		}
		/* Check if the Systolic blood pressure is over 140
		 * and if the Diastolic blood pressure is over 90
		 */
		if ((vital.getBloodPressureSystolic() >= 140) || 
				(vital.getBloodPressureDiastolic() >= 90)) {
			urgencyLevel++;
		}
		// Check if the Heart Rate of the Patient is not between 50 and 100
		if ((vital.getHeartRate() >= 100) || (vital.getHeartRate() <= 50)) {
			urgencyLevel++;
		}
		// Set the urgency level of the Patient based on emergency index
		switch (urgencyLevel) {
		case 1:
			urgencyCategory = "Non Urgent";
			break;
		case 2:
			urgencyCategory = "Less Urgent";
			break;
		case 3:
			urgencyCategory = "Urgent";
			break;
		case 4:
			urgencyCategory = "Urgent";
			break;
		default:
			urgencyCategory = "VItal info not available";
			break;

		}
		setUrgencyLevel(urgencyLevel);
	}
}