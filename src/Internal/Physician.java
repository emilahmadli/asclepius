package Internal;

import java.io.Serializable;

/**
 * Implementing the class Physician 
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew.
 */
public class Physician extends User implements Serializable {

	/** Initialize the variables. */
	/** A unique ID for serialization. */
	private static final long serialVersionUID = 7361324095407170797L;
	 /**
	  * Constructs the class Physician.
	  * @param username The username of the Physician.
	  * @param password The password of the Physician.
	  */
	public Physician(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	/**
	 * Adds the prescription to the specific Patient.
	 * @param p The Patient for whom the prescription is being added.
	 * @param medication The medication for the Patient.
	 * @param instruction The instruction for the Patient.
	 */
	public void addPrescription(Patient p, String medication, String instruction) {
		p.addPrescription(medication, instruction);
	}
	
	/**
	 * Searches for the Patient using Health Card Number.
	 * @param healthID Health Card Number of the Patient.
	 * @return the Patient using Health Card Number.
	 */
	public Patient searchPatient(String healthID) {
		return getRecord().getPatientbyID(healthID);
	}
}
