package Internal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementing the class Record
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew.
 */
public class Record implements Serializable {

	/**
	 * Initializes the variables.
	 */
	private static final long serialVersionUID = 5606995004465527093L;
	private Map<String, Patient> patients;
	private EmergencyRoom room;
	private Map<String, User> users;
	
	/**
	 * Returns a patient object associated with given healthID.
	 * @param healthID healthID of the patient to be searched.
	 * @return returns a patient object associated with ID.
	 */
	public Patient getPatientbyID(String healthID) {
		return patients.get(healthID);
	}
	
	/**
	 * Allows nurse to initiate a record.
	 */
	public Record() {
		patients = new HashMap<String, Patient>();
		room = new EmergencyRoom();
		users = new HashMap<String, User>();
		Nurse.setRecordTarget(this);
	}
	
	/**
	 * Returns the Map of all Patients.
	 * @return List of all users.
	 */
	public Map<String, User> getUserList()
	{
		return users;
	}
	
	/**
	 * Allows a nurse user to be saved in our record.
	 * @param nurse Nurse to be added.
	 */
	public void addNurse(Nurse nurse)
	{
		users.put(nurse.getUsername(), nurse);
	}
	
	/**
	 * Allows a physician user to be saved in our record.
	 * @param phy Physician to be added.
	 */
	public void addPhysician(Physician phy)
	{
		users.put(phy.getUsername(), phy);
	}
	
	/**
	 * Adds the Patients to Patient List.
	 * @param p allows the nurse to add patient to the record database.
	 */
	public void addPatient(Patient p) {
		patients.put(p.getHealthID(), p);
	}
	
	/**
	 * Returns the list of Patients not checked by Doctor.
	 * @return the list of Patients not checked by Doctor.
	 */
	public List<Patient> getUncheckedPatientsSorted() {
		List<Patient> uncheckedPatients = new ArrayList<Patient>();
		for (Patient p : patients.values()) {
			if (!p.getStatus()) {
				uncheckedPatients.add(p);
			}
		}
		Collections.sort(uncheckedPatients, new urgencyComparator());
		return uncheckedPatients;
	}
	// Compare urgencyLevel between two patients
	public class urgencyComparator implements Comparator<Patient> {
		@Override
		public int compare(Patient p1, Patient p2) {

			// First compare patient 1's urgencyLevel with patient 2's.
			if (p1.getUrgencyLevel() > p2.getUrgencyLevel()) {
				return -1;
			} else if (p1.getUrgencyLevel() < p2.getUrgencyLevel()) {
				return 1;
			} else {

				// If two patients have the same urgencyLevel, then compare 
				// them by arrival time and date.
				return p1.getArrivalTime().compareTo(p2.getArrivalTime());
			}

		}
	}
	/**
	 * Returns room returns an emergency room, where the nurse
	 * can check out patients listed by urgency or arrival.
	 * @return room returns an emergency room, where the nurse
	 * can check out patients listed by urgency or arrival.
	 */
	public EmergencyRoom getER() {
		return room;
	}

}
