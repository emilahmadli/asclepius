package Internal;

import java.io.*;
import java.util.Date;
import java.util.List;

/**
 * Implementing the class Nurse
 * @authors Emil Ahmadli, Tural Gulmammadov, Ka Ho Wu, Hong Zhi Yew. 
 */
public class Nurse extends User implements Serializable {
	/** Initialize the variables. */
	/** A unique ID for serialization. */
	private static final long serialVersionUID = 532067736378228105L;
	
	/**
	 * Constructs the class Nurse.
	 * @param username The username of the Nurse.
	 * @param password The password of the Nurse.
	 */
	public Nurse(String username, String password)
	{
		this.setUsername(username);
		this.password = password;
	}
	
	/**
	 * The Nurse adds patient to the Emergency Room.
	 * @param name Name of the Patient.
	 * @param dob Date of birth of the Patient.
	 * @param healthID Health Card Number of the Patient.
	 */
	public void addPatient(String name, Date dob, 
			String healthID) 
	{
		Patient p = new Patient(name, dob, healthID);
		getRecord().addPatient(p);
	}
	
	/**
	 * Record symptoms of the Patient.
	 * @param p The Patient.
	 * @param s The symptom of the Patient.
	 */
	public void recordSymptoms(Patient p, String s)
	{
		p.setSymptoms(s);
	}
	
	/**
	 * Records the Vital Signs of the Patient.
	 * @param p The Patient.
	 * @param temp The temperature of the Patient.
	 * @param blood1 The systolic blood pressure of the Patient.
	 * @param blood2 The diastolic blood pressure of the Patient.
	 * @param heart The heart rate of the Patient.
	 */
	public void recordVital(Patient p, double temp, double blood1, 
			double blood2, double heart)
	{
		p.addVital(temp, blood1, blood2, heart);
	}
	
	/**
	 * Records the Doctor who checked the Patient.
	 * @param p The Patient.
	 */
	public void recordDoctorVisit(Patient p)
	{
		p.checkedBy();
		p.setStatus(true);
	}
	
	/**
	 * Adds patient to the Emergency Room.
	 * @param p The Patient.
	 */
	public void addPatientToER(Patient p)
	{
		getRecord().getER().addPatientToER(p);
	}
	
	/**
	 * Returns the list of Patients sorted by their urgency level.
	 * @return the list of Patients sorted by their urgency level.
	 */
	public List<Patient> listByUrgency()
	{
		return getRecord().getER().listByUrgency();
	}

	/**
	 * Returns the list of Patients sorted by their arrival time.
	 * @return the list of Patients sorted by their arrival time.
	 */
	public List<Patient> listByArrival()
	{
		return getRecord().getER().listByArrival();
	}
	
	/**
	 * Searches for the Patient using Health Card Number.
	 * @param healthID Health Card Number of the Patient.
	 * @return the Patient using Health Card Number.
	 */
	public Patient searchPatient(String healthID) {
		return getRecord().getPatientbyID(healthID);
	}
	
	/**
	 * Marks a patient as already seen by a doctor.
	 * @param p Patient to be marked.
	 */
	public void patientSeenByDoc(Patient p) {
		p.checkedBy();
	}
	
	/**
	 * Returns the List of Patients not checked by doctor.
	 * @return List of patients that are not already checked.
	 */
	public List<Patient> listPatientsUnchecked() {
		return getRecord().getUncheckedPatientsSorted();
	}
}
