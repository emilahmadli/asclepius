package Internal;

import java.io.Serializable;

public abstract class User implements Serializable {

	/**
	 * The User class, which will be inherited by the classes Nurse and Physician.
	 */
	private static final long serialVersionUID = 435326358097035086L;
	protected String username, password;
	private static Record record;
	
	/**
	 * Allows the User to load a certain record.
	 * @param record that the User loads.
	 */
	public static void setRecordTarget(Record record)
	{
		User.record = record;
	}
	
	/**
	 * Logs in the User, using the password.
	 * @param password the Password for the account.
	 * @return True if the login is successful, false otherwise.
	 */
	public boolean login(String password)
	{
		return this.password.toString().compareTo(password) == 0;
	}

	/**
	 * Returns the username of the User.
	 * @return the username of the User.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Returns the record of the Nurse.
	 * @return the record of the Nurse.
	 */
	public static Record getRecord()
	{
		return record;
	}

	/**
	 * Sets username of the User.
	 * @param username the username to set.
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	public abstract Patient searchPatient(String name);

}
